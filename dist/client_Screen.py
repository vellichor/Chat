# -*- coding: utf-8 -*-
import Tkinter
from Tkinter import *
import ttk
from PIL import Image
import socket
import threading
import sys
import pickle
from PIL import ImageGrab
import time
import os 
import Tix as tk


class Client():

	def on_entry_click(self,event):
		if self.NameText.get() == 'enter your name':
			self.NameText.delete(0, "end") 
			self.NameText.insert(0, '') 
			self.NameText.config(fg = 'black')


	def on_focusout(self,event):
		if self.NameText.get() == '':
			self.NameText.insert(0, 'enter your name')
			self.NameText.config(fg = 'grey')


	def on_entry_clickIp(self,event):
		if self.IpText.get() == 'enter ip to connect':
			self.IpText.delete(0, "end") 
			self.IpText.insert(0, '') 
			self.IpText.config(fg = 'black')


	def on_focusoutIp(self,event):
		if self.IpText.get() == '':
			self.IpText.insert(0, 'enter ip to connect')
			self.IpText.config(fg = 'grey')


	def screenshotSend(self,event):
		state=str(self.buttonScreen['state'])
		if state == NORMAL:
			self.CheckSend=True
			self.buttonScreen.configure(state="disabled")
			self.buttonScreen.update()
			self.sock.send(pickle.dumps("screenshot"))
			self.MessageTextBox.configure(state='normal')
			self.MessageTextBox.insert("1.0","\n3\n")
			self.MessageTextBox.configure(state='disabled')
			self.MessageTextBox.update()
			print 3
			time.sleep(1)
			self.MessageTextBox.configure(state='normal')
			self.MessageTextBox.insert("1.0","\n2")
			self.MessageTextBox.configure(state='disabled')
			self.MessageTextBox.update()
			print 2
			time.sleep(1)
			self.MessageTextBox.configure(state='normal')
			self.MessageTextBox.insert("1.0","\n1")
			self.MessageTextBox.configure(state='disabled')
			self.MessageTextBox.update()
			print 1
			time.sleep(1)
			self.screenCounterSend+=1
			ImageGrab.grab().save("%s.jpg" %self.screenCounterSend, "JPEG")
			f = open('%s.jpg' %self.screenCounterSend,'rb')
			screenshotFile = f.read(1024)
			counter=0
			print 'sending'
			self.MessageTextBox.configure(state='normal')
			self.TextBox.delete(0,END)
			self.MessageTextBox.insert("1.0","sending "+"\n")
			self.MessageTextBox.configure(state='disabled')
			while screenshotFile:
				self.sock.send(screenshotFile)
				screenshotFile = f.read(1024)
				time.sleep(0.01)
				sys.stdout.write('.')
				#time.sleep(0.01)
				self.MessageTextBox.configure(state='normal')
				self.TextBox.delete(0,END)
				#print input
				self.MessageTextBox.insert("1.0",".")
				self.MessageTextBox.configure(state='disabled')
				self.MessageTextBox.update()
				counter+=1;
			f.close()
			time.sleep(0.1)
			self.sock.send("this-is-done")
			print "done"
			self.buttonScreen.configure(state="normal")
			self.buttonScreen.update()



	def screenshotOpenYours(self,event):
		print "opening last ours"
		os.system("start %s.jpg" %self.screenCounterSend)


	def screenshotOpen(self,event):
		print "opening last"
		os.system("start mine%s.jpg" %self.screenCounter)





	def __init__(self, host="", port=4000):

		self.app = Tk()
		self.app.configure(background='#2196F3')


		self.top = Frame(self.app,background='#2196F3')
		self.bottom = Frame(self.app,background='#2196F3')
		self.top.pack(side=TOP,fill=BOTH)
		self.bottom.pack(side=BOTTOM, fill=BOTH)

		self.app.title("מה קורס מגשימים הודעות")
		self.app.resizable(width=False, height=False)
		self.screenCounterSend=0
		self.screenCounter=0
		self.NameText = Entry(self.app, font=("Arial",12),justify=RIGHT)
		self.NameText.insert(0, 'enter your name')
		self.NameText.bind('<FocusIn>', self.on_entry_click)
		self.NameText.bind('<FocusOut>', self.on_focusout)
		self.NameText.config(fg = 'grey')
		self.NameText.pack(fill="x")

		self.IpText = Entry(self.app, font=("Arial",12),justify=RIGHT)
		self.IpText.insert(0, 'enter ip to connect')
		self.IpText.bind('<FocusIn>', self.on_entry_clickIp)
		self.IpText.bind('<FocusOut>', self.on_focusoutIp)
		self.IpText.config(fg = 'grey')
		self.IpText.pack(fill="x")


		self.buttonName = Button(self.app, text="Save your name and Ip",bg="#0D47A1", width=10, height=2,font=("Arial",12,"bold"),fg="white")
		self.buttonName.bind('<Button-1>',self.nameSave)
		self.buttonName.pack(fill="x")

		self.MessageTextBox = Text(self.app, height=20, width=50, font=("Arial",11),state='disabled', wrap='word')
		self.MessageTextBox.pack(fill=BOTH,pady=20)


		self.TextBox = Entry(self.app, width=30, font=("Arial",12),justify=RIGHT)
		self.TextBox.pack(fill="x")
		self.app.bind("<Return>", self.PrintYourMessage)


		self.buttonSend = Button(self.app, text="send",bg="#0D47A1", width=10, height=2,font=("Arial",12,"bold"),fg="white")
		self.buttonSend.bind('<Button-1>',self.PrintYourMessage)
		self.buttonSend.pack(fill="x")


		self.buttonScreen = Button(self.app, text="send Screenshot",bg="#0D47A1", width=30, height=2,font=("Arial",12,"bold"),fg="white")
		
		self.buttonScreen.bind('<Button-1>',self.screenshotSend)
		self.buttonScreen.pack(fill="x")

		self.buttonOpen = Button(self.app, text="open last screenshot sent",bg="#0D47A1", width=30, height=2,font=("Arial",12,"bold"),fg="white")
		self.buttonOpen.bind('<Button-1>',self.screenshotOpen)
		self.buttonOpen.pack(fill="x",side="left")


		self.buttonOpenYours = Button(self.app, text="open yours last screenshot",bg="#0D47A1", width=30, height=2,font=("Arial",12,"bold"),fg="white")
		self.buttonOpenYours.bind('<Button-1>',self.screenshotOpenYours)
		self.buttonOpenYours.pack(fill="x",side="right")


		self.app.mainloop()
		self.app.withdraw()
		




	def msg_recv(self):
		while True:
			try:
				data = self.sock.recv(1024)
				if data:
					if pickle.loads(data)=='screenshot':
						self.screenCounter+=1
						self.buttonScreen.config(state=DISABLED)
						with open('mine%s.jpg' %self.screenCounter, 'wb') as f:
							try:
								print 'screenshot ->'
								screenshotFile = self.sock.recv(1024)
								counter = 1
								self.MessageTextBox.configure(state='normal')
								self.TextBox.delete(0,END)
								self.MessageTextBox.insert("1.0","getting "+"\n")
								self.MessageTextBox.configure(state='disabled')
								while "this-is-done" not in screenshotFile:
									try:
										f.write(screenshotFile)
										time.sleep(0.01)
										self.MessageTextBox.configure(state='normal')
										self.TextBox.delete(0,END)
										self.MessageTextBox.insert("1.0",".")
										self.MessageTextBox.configure(state='disabled')
										sys.stdout.write('.')
										screenshotFile = self.sock.recv(1024)
										counter += 1
									except:
										screenshotFile="this-is-done"
								f.close()
								print "done"
								os.system("start mine%s.jpg" %self.screenCounter)
								sys.stdout.write('->')
								self.buttonScreen.config(state=NORMAL)
							except:
								f.close()
					else:
						print(pickle.loads(data))
						self.MessageTextBox.configure(state='normal')
						#self.TextBox.delete(0,END)
						#print input
						self.MessageTextBox.insert("1.0",pickle.loads(data)+"\n")
						self.MessageTextBox.configure(state='disabled')
						sys.stdout.write('->')
			except:
				pass


	def nameSave(self,event):
		self.NameText.configure(state='disabled')
		self.IpText.configure(state='disabled')
		self.host=self.IpText.get()
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.sock.connect((str(self.host), 4000))
		self.sock.settimeout(5)
		msg_recv = threading.Thread(target=self.msg_recv)
		msg_recv.daemon = True
		msg_recv.start()
		name=self.NameText.get()
		self.sock.send(pickle.dumps(name+" is connected"))


	def PrintYourMessage(self,event):
		name = self.NameText.get()
		input1 = self.TextBox.get()
		if len(input1)>512:
			self.MessageTextBox.configure(state='normal')
			self.TextBox.delete(0,END)
			self.MessageTextBox.insert("1.0","The length is bigger than you can write please enter under 512 charecters"+"\n")
			self.MessageTextBox.configure(state='disabled')
		if input1!='' and name!='enter your name' and name!='' and len(input1)<512:
			self.MessageTextBox.configure(state='normal')
			self.TextBox.delete(0,END)
			self.MessageTextBox.insert("1.0","You : "+input1+"\n")
			self.MessageTextBox.configure(state='disabled')
			dataToSend=name+": "+input1
			self.sock.send(pickle.dumps(dataToSend.encode('utf-8')))
c = Client()

