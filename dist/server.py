import socket
import threading
import sys
import pickle
from PIL import ImageGrab
import time

class server():
	"""wating for server up"""
	def __init__(self, host="0.0.0.0", port=4000):

		self.clientes = []

		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.sock.bind((str(host), int(port)))
		self.sock.listen(10)
		self.sock.setblocking(0)

		accept = threading.Thread(target=self.aceptarCon)
		process = threading.Thread(target=self.procesarCon)
		
		accept.daemon = True
		accept.start()

		process.daemon = True
		process.start()

		while True:
			msg = input('->')
			if msg == 'exit':
				self.sock.close()
				sys.exit()
			else:
				pass


	def msg_to_all(self, msg, cliente):
		for c in self.clientes:
			try:
				#time.sleep(1)
				if c != cliente:
					#time.sleep(0.01)
					c.send(msg)
					#c.send(msg)
					#print (msg)
			except:
				self.clientes.remove(c)
		
	def aceptarCon(self):
		print("accept started")
		while True:
			try:
				conn, addr = self.sock.accept()
				conn.setblocking(False)
				self.clientes.append(conn)
			except:
				pass

	def procesarCon(self):
		print("process started with:")
		counter=0
		while True:
			if len(self.clientes) > 0:
				for c in self.clientes:
					try:
						data = c.recv(1024)
						if data:
							#counter+=1
							#print counter
							time.sleep(0.01)
							self.msg_to_all(data,c)
					except:
						pass


s = server()